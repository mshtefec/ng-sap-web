import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BackComponent } from './back.component';
import { BackRoutingModule } from './back-routing.module';

import { SidebarComponent } from '../sidebar/sidebar.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { FooterComponent } from '../footer/footer.component';
import { BlankComponent } from '../blank/blank.component';

import { TaskListComponent } from './tasks/task-list/task-list.component';
import { TaskFormComponent } from './tasks/task-form/task-form.component';
import { ClientFormComponent } from './clients/client-form/client-form.component';
import { ClientListComponent } from './clients/client-list/client-list.component';
import { AccountListComponent } from './accounts/account-list/account-list.component';
import { AccountFormComponent } from './accounts/account-form/account-form.component';
import { ReportListComponent } from './reports/report-list/report-list.component';
import { EmployeeListComponent } from './staff/employee-list/employee-list.component';
import { EmployeeFormComponent } from './staff/employee-form/employee-form.component';
import { RoleFormComponent } from './roles/role-form/role-form.component';
import { RoleListComponent } from './roles/role-list/role-list.component';
import { ProductFormComponent } from './products/product-form/product-form.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { SaleFormComponent } from './sales/sale-form/sale-form.component';
import { SaleListComponent } from './sales/sale-list/sale-list.component';
import { TypeTaskFormComponent } from './types-tasks/type-task-form/type-task-form.component';
import { TypeTaskListComponent } from './types-tasks/type-task-list/type-task-list.component';
import { TypeProductListComponent } from './products/type-product-list/type-product-list.component';
import { TypeProductFormComponent } from './products/type-product-form/type-product-form.component';

@NgModule({
    imports: [
      CommonModule,
      BackRoutingModule,
      FormsModule,
      ReactiveFormsModule
    ],
    declarations: [
      BackComponent,
      BlankComponent,
      SidebarComponent,
      NavbarComponent,
      FooterComponent,
      TaskListComponent,
      TaskFormComponent,
      ClientFormComponent,
      ClientListComponent,
      AccountListComponent,
      AccountFormComponent,
      ReportListComponent,
      EmployeeListComponent,
      EmployeeFormComponent,
      RoleFormComponent,
      RoleListComponent,
      ProductFormComponent,
      ProductListComponent,
      SaleFormComponent,
      SaleListComponent,
      TypeTaskFormComponent,
      TypeTaskListComponent,
      TypeProductListComponent,
      TypeProductFormComponent
    ]
  })
  export class BackModule { }