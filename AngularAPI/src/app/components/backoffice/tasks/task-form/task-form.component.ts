import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { ToastrService } from 'ngx-toastr';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit {

  constructor(
    private service: TaskService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.resetForm();
  }
  
  onSubmit(form: NgForm) {
    console.log(form.value);
    if (form.value.id == null)
      this.insertRecord(form);
    else
      this.updateRecord(form);
  }

  resetForm(form?: NgForm) {
    if(form != null){
      form.resetForm();
      this.service.selectedTask = new Task();
    }
  }

  insertRecord(form: NgForm) {
    this.service.postTask(form.value).subscribe(res => {
      this.toastr.success('Inserted successfully', 'TASK. Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

  updateRecord(form: NgForm) {
    this.service.putTask(form.value).subscribe(res => {
      this.toastr.info('Updated successfully', 'TASK. Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

}
