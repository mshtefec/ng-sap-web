import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../../../services/auth.service';
import { TaskService } from 'src/app/services/task.service';

import { Task } from '../../../../models/task.model';
import { TargetLocator } from 'selenium-webdriver';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private authService: AuthService,
    private service: TaskService
  ) { }
  
  public isAdmin: any = null;
  public isChief: any = null;
  public isTrader: any = null;
  public isTechnical: any = null;

  public userUid: string = null;

  ngOnInit() {
    this.getCurrentUser();
    this.service.refreshList();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserRoles(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
          this.isChief = Object.assign({}, userRole.roles).hasOwnProperty('chief');
          this.isTrader = Object.assign({}, userRole.roles).hasOwnProperty('trader');
          this.isTechnical = Object.assign({}, userRole.roles).hasOwnProperty('technical');
        })
      }
    });
  }

  updateTask(task: Task) {
    
    task.estado = 'finalizada';

    this.service.putTask(task).subscribe(res => {
      this.toastr.info('Updated successfully', 'TASK. Update');
      this.service.refreshList();
    });
  }

}
