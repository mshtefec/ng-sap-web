import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeTaskListComponent } from './type-task-list.component';

describe('TypeTaskListComponent', () => {
  let component: TypeTaskListComponent;
  let fixture: ComponentFixture<TypeTaskListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeTaskListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeTaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
