import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeTaskFormComponent } from './type-task-form.component';

describe('TypeTaskFormComponent', () => {
  let component: TypeTaskFormComponent;
  let fixture: ComponentFixture<TypeTaskFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeTaskFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeTaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
