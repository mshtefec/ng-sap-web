import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClientService } from 'src/app/services/client.service';
import { ToastrService } from 'ngx-toastr';
import { Client } from 'src/app/models/client.model';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss']
})
export class ClientFormComponent implements OnInit {

  constructor(
    private service: ClientService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.resetForm();
  }
  
  onSubmit(form: NgForm) {
    //console.log(form.value);
    if (form.value.$nro_cliente == null)
      this.insertRecord(form);
    else
      this.updateRecord(form);
  }

  resetForm(form?: NgForm) {
    if(form != null){
      form.resetForm();
      this.service.selectedClient = new Client();
    }
  }

  insertRecord(form: NgForm) {
    this.service.postClient(form.value).subscribe(res => {
      this.toastr.success('Inserted successfully', 'EMP. Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

  updateRecord(form: NgForm) {
    this.service.putClient(form.value).subscribe(res => {
      this.toastr.info('Updated successfully', 'EMP. Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

}
