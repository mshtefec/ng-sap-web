import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BackComponent } from './back.component';
import { BlankComponent } from '../blank/blank.component';

import { TypeTaskListComponent } from './types-tasks/type-task-list/type-task-list.component';
import { TypeTaskFormComponent } from './types-tasks/type-task-form/type-task-form.component';

import { TaskListComponent } from './tasks/task-list/task-list.component';
import { TaskFormComponent } from './tasks/task-form/task-form.component';

import { TypeProductListComponent } from './products/type-product-list/type-product-list.component';
import { TypeProductFormComponent } from './products/type-product-form/type-product-form.component';

import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductFormComponent } from './products/product-form/product-form.component';

import { ClientListComponent } from './clients/client-list/client-list.component';
import { ClientFormComponent } from './clients/client-form/client-form.component';

import { AccountListComponent } from './accounts/account-list/account-list.component';
import { AccountFormComponent } from './accounts/account-form/account-form.component';

import { ReportListComponent } from './reports/report-list/report-list.component';

import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
  { 
    path: 'admin', 
    component: BackComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: BlankComponent },
      { path: '', redirectTo: 'admin', pathMatch: 'full' },
      { path: 'tasks', component: TaskListComponent },
      { path: 'tasks/new', component: TaskFormComponent },
      { path: 'types-tasks', component: TypeTaskListComponent },
      { path: 'types-tasks/new', component: TypeTaskFormComponent },
      { path: 'clients', component: ClientListComponent },
      { path: 'clients/new', component: ClientFormComponent },
      { path: 'products', component: ProductListComponent },
      { path: 'products/new', component: ProductFormComponent },
      { path: 'types-products', component: TypeProductListComponent },
      { path: 'types-products/new', component: TypeProductFormComponent },
      { path: 'accounts', component: AccountListComponent },
      { path: 'accounts/new', component: AccountFormComponent },
      { path: 'reports', component: ReportListComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackRoutingModule { }