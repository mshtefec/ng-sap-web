import { Injectable } from '@angular/core';
import { Task } from '../models/task.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  formData: Task;
  list : Task[];
  selectedTask: Task = new Task();
  readonly rootUrl = "https://localhost:44347/api";

  constructor(private http: HttpClient) { }

  postTask(formData: Task) {
    formData.fecha = new Date();
    return this.http.post(this.rootUrl + '/Tareas', formData)
    console.log(this.http.post(this.rootUrl + '/Tareas', formData));
  }

  refreshList() {
    this.http.get(this.rootUrl + '/Tareas')
    .toPromise().then(res => this.list = res as Task[]);
  }

  putTask(formData: Task) {
    return this.http.put(this.rootUrl + '/Tareas/' + formData.id, formData);
  }

  deleteTask(id: number) {
    return this.http.delete(this.rootUrl + '/Tareas/' + id);
  }

  getTask(id: number) {
    return this.http.get(this.rootUrl + '/Tareas/' + id).toPromise().then(res => this.formData = res as Task);
  }
}
