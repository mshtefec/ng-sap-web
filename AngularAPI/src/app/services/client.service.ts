import { Injectable } from '@angular/core';
import { Client } from '../models/client.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  formData: Client;
  list : Client[];
  selectedClient: Client = new Client();
  readonly rootUrl = "https://localhost:44347/api";

  constructor(private http: HttpClient) { }

  postClient(formData: Client) {
    return this.http.post(this.rootUrl + '/Clientes', formData)
    console.log(this.http.post(this.rootUrl + '/Clientes', formData));
  }

  refreshList(){
    this.http.get(this.rootUrl + '/Clientes')
    .toPromise().then(res => this.list = res as Client[]);
  }

  putClient(formData : Client){
    return this.http.put(this.rootUrl + '/Clientes/' + formData.$nro_cliente, formData);
  }

  deleteClient(id : number){
    return this.http.delete(this.rootUrl + '/Clientes/' + id);
  }
}
