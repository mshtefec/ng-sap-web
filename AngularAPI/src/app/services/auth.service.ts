import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { map } from 'rxjs/operators'

import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Employee } from '../models/employee.model';

@Injectable()
export class AuthService {

  constructor(
    public afsAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) { }

  loginTwitter () {
    return this.afsAuth.auth.signInWithPopup( new firebase.auth.TwitterAuthProvider());
  }

  loginFacebook() {
    return this.afsAuth.auth.signInWithPopup( new firebase.auth.FacebookAuthProvider());
  }

  loginGoogle() {
    return this.afsAuth.auth.signInWithPopup( new firebase.auth.GoogleAuthProvider());
  }

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth.createUserWithEmailAndPassword(email, pass)
        .then(userData => {
          resolve(userData),
            this.updateUserData(userData.user)
        }).catch(err => console.log(reject(err)))
    });
  }

  loginEmail(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth.signInWithEmailAndPassword(email, pass)
      .then( userData =>  resolve(userData),
      err => reject (err));
    });
  }

  updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`employees/${user.uid}`);
    const data: Employee = {
      id: user.uid,
      email: user.email,
      roles: {
        trader: true,
      }
    }
    return userRef.set(data, { merge: true })
  }

  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }

  isUserRoles(userUid) {
    return this.afs.doc<Employee>(`employees/${userUid}`).valueChanges();
  }

  logout() {
    return this.afsAuth.auth.signOut();
  }

}