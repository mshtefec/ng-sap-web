export interface Role {
    admin?: boolean;
    trader?: boolean;
    technical?: boolean;
}