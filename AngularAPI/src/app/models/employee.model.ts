import { Role } from './role.model';

export class Employee {
    id?: string;
    name?: string;
    email?: string;
    password?: string;
    roles?: Role;
}