export class Task {
    id?: number;
    cuenta?: number;
    tipo?:  string;
    dni_tecnico?:  number;
    descripcion?: string;
    estado?: string;
    fecha?: Date;
}
