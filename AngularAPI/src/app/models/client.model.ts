export class Client {
    $nro_cliente: number;
    apellido: string;
    nombre: string;
    dni: number;
    fecha_nac: Date;
    provincia: string;
    ciudad: string;
    celular: number;
    telefono: number;
    email: string;
    calle: string;
    nro_calle: number;
    dpto: string;
    piso: number;
    barrio: string;
    manzana: number;
    parcela: number;

    constructor(apellido?: string, nombre?: string, dni?: number) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.dni = dni;
    }
}
