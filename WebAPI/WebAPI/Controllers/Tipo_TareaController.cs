﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Tipo_TareaController : ApiController
    {
        private DBTipo_Tarea db = new DBTipo_Tarea();

        // GET: api/Tipo_Tarea
        public IQueryable<Tipo_Tarea> GetTipo_Tarea()
        {
            return db.Tipo_Tarea;
        }

        // GET: api/Tipo_Tarea/5
        [ResponseType(typeof(Tipo_Tarea))]
        public IHttpActionResult GetTipo_Tarea(string id)
        {
            Tipo_Tarea tipo_Tarea = db.Tipo_Tarea.Find(id);
            if (tipo_Tarea == null)
            {
                return NotFound();
            }

            return Ok(tipo_Tarea);
        }

        // PUT: api/Tipo_Tarea/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipo_Tarea(string id, Tipo_Tarea tipo_Tarea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipo_Tarea.tipo)
            {
                return BadRequest();
            }

            db.Entry(tipo_Tarea).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Tipo_TareaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tipo_Tarea
        [ResponseType(typeof(Tipo_Tarea))]
        public IHttpActionResult PostTipo_Tarea(Tipo_Tarea tipo_Tarea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tipo_Tarea.Add(tipo_Tarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (Tipo_TareaExists(tipo_Tarea.tipo))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tipo_Tarea.tipo }, tipo_Tarea);
        }

        // DELETE: api/Tipo_Tarea/5
        [ResponseType(typeof(Tipo_Tarea))]
        public IHttpActionResult DeleteTipo_Tarea(string id)
        {
            Tipo_Tarea tipo_Tarea = db.Tipo_Tarea.Find(id);
            if (tipo_Tarea == null)
            {
                return NotFound();
            }

            db.Tipo_Tarea.Remove(tipo_Tarea);
            db.SaveChanges();

            return Ok(tipo_Tarea);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Tipo_TareaExists(string id)
        {
            return db.Tipo_Tarea.Count(e => e.tipo == id) > 0;
        }
    }
}