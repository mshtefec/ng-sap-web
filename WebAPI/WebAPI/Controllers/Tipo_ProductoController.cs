﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Tipo_ProductoController : ApiController
    {
        private DBTipo_Producto db = new DBTipo_Producto();

        // GET: api/Tipo_Producto
        public IQueryable<Tipo_Producto> GetTipo_Producto()
        {
            return db.Tipo_Producto;
        }

        // GET: api/Tipo_Producto/5
        [ResponseType(typeof(Tipo_Producto))]
        public IHttpActionResult GetTipo_Producto(int id)
        {
            Tipo_Producto tipo_Producto = db.Tipo_Producto.Find(id);
            if (tipo_Producto == null)
            {
                return NotFound();
            }

            return Ok(tipo_Producto);
        }

        // PUT: api/Tipo_Producto/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipo_Producto(int id, Tipo_Producto tipo_Producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipo_Producto.id)
            {
                return BadRequest();
            }

            db.Entry(tipo_Producto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Tipo_ProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tipo_Producto
        [ResponseType(typeof(Tipo_Producto))]
        public IHttpActionResult PostTipo_Producto(Tipo_Producto tipo_Producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tipo_Producto.Add(tipo_Producto);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipo_Producto.id }, tipo_Producto);
        }

        // DELETE: api/Tipo_Producto/5
        [ResponseType(typeof(Tipo_Producto))]
        public IHttpActionResult DeleteTipo_Producto(int id)
        {
            Tipo_Producto tipo_Producto = db.Tipo_Producto.Find(id);
            if (tipo_Producto == null)
            {
                return NotFound();
            }

            db.Tipo_Producto.Remove(tipo_Producto);
            db.SaveChanges();

            return Ok(tipo_Producto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Tipo_ProductoExists(int id)
        {
            return db.Tipo_Producto.Count(e => e.id == id) > 0;
        }
    }
}