﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ComercialesController : ApiController
    {
        private DBComercial db = new DBComercial();

        // GET: api/Comerciales
        public IQueryable<Comercial> GetComercials()
        {
            return db.Comercials;
        }

        // GET: api/Comerciales/5
        [ResponseType(typeof(Comercial))]
        public IHttpActionResult GetComercial(int id)
        {
            Comercial comercial = db.Comercials.Find(id);
            if (comercial == null)
            {
                return NotFound();
            }

            return Ok(comercial);
        }

        // PUT: api/Comerciales/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutComercial(int id, Comercial comercial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comercial.dni)
            {
                return BadRequest();
            }

            db.Entry(comercial).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComercialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Comerciales
        [ResponseType(typeof(Comercial))]
        public IHttpActionResult PostComercial(Comercial comercial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Comercials.Add(comercial);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ComercialExists(comercial.dni))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = comercial.dni }, comercial);
        }

        // DELETE: api/Comerciales/5
        [ResponseType(typeof(Comercial))]
        public IHttpActionResult DeleteComercial(int id)
        {
            Comercial comercial = db.Comercials.Find(id);
            if (comercial == null)
            {
                return NotFound();
            }

            db.Comercials.Remove(comercial);
            db.SaveChanges();

            return Ok(comercial);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ComercialExists(int id)
        {
            return db.Comercials.Count(e => e.dni == id) > 0;
        }
    }
}