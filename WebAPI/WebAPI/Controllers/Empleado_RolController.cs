﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Empleado_RolController : ApiController
    {
        private DBEmpleadoRol db = new DBEmpleadoRol();

        // GET: api/Empleado_Rol
        public IQueryable<Empleado_Rol> GetEmpleado_Rol()
        {
            return db.Empleado_Rol;
        }

        // GET: api/Empleado_Rol/5
        [ResponseType(typeof(Empleado_Rol))]
        public IHttpActionResult GetEmpleado_Rol(int id)
        {
            Empleado_Rol empleado_Rol = db.Empleado_Rol.Find(id);
            if (empleado_Rol == null)
            {
                return NotFound();
            }

            return Ok(empleado_Rol);
        }

        // PUT: api/Empleado_Rol/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmpleado_Rol(int id, Empleado_Rol empleado_Rol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != empleado_Rol.dni_empleado)
            {
                return BadRequest();
            }

            db.Entry(empleado_Rol).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Empleado_RolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Empleado_Rol
        [ResponseType(typeof(Empleado_Rol))]
        public IHttpActionResult PostEmpleado_Rol(Empleado_Rol empleado_Rol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Empleado_Rol.Add(empleado_Rol);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (Empleado_RolExists(empleado_Rol.dni_empleado))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = empleado_Rol.dni_empleado }, empleado_Rol);
        }

        // DELETE: api/Empleado_Rol/5
        [ResponseType(typeof(Empleado_Rol))]
        public IHttpActionResult DeleteEmpleado_Rol(int id)
        {
            Empleado_Rol empleado_Rol = db.Empleado_Rol.Find(id);
            if (empleado_Rol == null)
            {
                return NotFound();
            }

            db.Empleado_Rol.Remove(empleado_Rol);
            db.SaveChanges();

            return Ok(empleado_Rol);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Empleado_RolExists(int id)
        {
            return db.Empleado_Rol.Count(e => e.dni_empleado == id) > 0;
        }
    }
}